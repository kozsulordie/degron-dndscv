import patsy
from statsmodels.discrete.discrete_model import NegativeBinomial


def glm(data, formula, verbose=False):
    """
    Args:
        formula: regression formula of the form: y ~ param_1 + param_2 + param_3 + ... + offset - 1
        BE CAREFUL TO PLACE OFFSET AS LAST COLUMN
    Returns:
        carries out regression fitting; it extends the regression data frame with parameters mu and a computed
        upon the regression model fit
    """
    y, x = patsy.dmatrices(formula, data)
    count_model = NegativeBinomial(y, x, loglike_method='nb2', offset=data['offset'].values)
    fitting = count_model.fit_regularized(disp=verbose)
    beta = fitting.params[:-2]
    data['mu'] = fitting.predict()
    data['a'] = fitting.params[-1]
    return data, fitting


if __name__ == '__main__':
    pass
