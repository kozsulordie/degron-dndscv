# python=3.6

"""Run estimate of dNdS in degrons"""

# imports

import os
import sys
import pandas as pd
import numpy as np
import dill as pickle
import gzip
import glob
import tqdm
from functools import partial
from itertools import product
from collections import defaultdict
import warnings
warnings.filterwarnings('ignore')

from neg_binom import glm
from dnds_degron import dNdSAnalysisDegron
from utils import mutkeygen, maf2profile, normalize, relative, all_contexts, transform


# constants

csqn_dict = {'synonymous_variant'     : 'syn',
             'missense_variant'       : 'mis',
             'stop_gained'            : 'other',
             'splice_acceptor_variant': 'other',
             'splice_donor_variant'   : 'other'}

PATH = '/workspace/projects/ubiquitins/degron-dndscv/data'
DEGRONPATH = '/workspace/projects/ubiquitins/run_tcga_pan/intogen_runs/20181105/dndscv/'

# counts per gene
with gzip.open(os.path.join(PATH, 'site_counts.pickle.gz'), 'rb') as g:
    genecount = pickle.load(g)

# counts per degron
with gzip.open(os.path.join(DEGRONPATH, 'dict_info_context.pickle.gz'), 'rb') as f:
    degroncount = pickle.load(f)
degron_site_counts = transform(degroncount)

# hugo-ensembl dict
with open(os.path.join(PATH, 'HGNC_ENSEMBL_dict.pickle'), 'rb') as h:
    hugo2ensembl = pickle.load(h)

# triplet counts for CDS
with gzip.open(os.path.join(PATH, 'tnt_counts_dict.pickle.gz'), 'rb') as f:
    CDS_COUNTS = pickle.load(f)['cds']

# Negative binomial regression covariates table
covtable = pd.read_csv(os.path.join(PATH, 'covariates.table.tsv'), sep='\t', index_col=0)


def run(tumortype):

    # mutations dataframe

    mafpath = os.path.join(DEGRONPATH, f'{tumortype}_annotmuts_degron.out.gz')
    maf = pd.read_csv(mafpath, sep='\t')

    # dnds output

    genemuts = pd.read_csv(os.path.join(DEGRONPATH, f'PCATLAS_WXS_{tumortype}_genemuts.out.gz'), sep='\t')
    dndsout  = pd.read_csv(os.path.join(DEGRONPATH, f'PCATLAS_WXS_{tumortype}.out.gz'), sep='\t')

    # estimate mutations profile

    profile = maf2profile(mafpath)
    nprofile = normalize(profile, CDS_COUNTS)
    relfreq = relative(nprofile)

    # expected degron/gene mutation intensity ratio

    degron2gene = {}
    for degron in degroncount:
        degron2gene[degron] = degron.split(':')[-1]

    # Negative binomial regression (NBR)

    nbrtable = covtable.copy()
    nbrtable = nbrtable[nbrtable.index.isin(genemuts['gene_name'])]
    nbrtable['n_syn'] = nbrtable.index.map(lambda v: genemuts.loc[genemuts['gene_name'] == v, 'n_syn'].values[0])
    nbrtable['exp_syn'] = nbrtable.index.map(lambda v: genemuts.loc[genemuts['gene_name'] == v, 'exp_syn'].values[0])
    nbrtable['offset'] = nbrtable['exp_syn'].apply(np.log)
    nbrtable.head()

    # ...append the degrons to NBR table

    concatrows = []
    for k in degron2gene:
        df = pd.DataFrame(nbrtable.loc[degron2gene[k], :]).transpose()
        df.index = [k]
        concatrows.append(df)
    degrons_table = pd.concat(concatrows)

    # ...update synonymous mutations in degrons

    degron_syn = maf[maf['degron'] != '-']
    for degron in degrons_table.index:
        gene = degron2gene[degron]
        degrons_table.loc[degron, 'n_syn'] = len(degron_syn[degron_syn['gene'] == gene])

    # ...update expected synonymous and offset

    partial_degron_rate = partial(degron_rate, relfreq=relfreq, degron2gene=degron2gene)
    degrons_table['degron_rate'] = degrons_table.index.map(partial_degron_rate)
    degrons_table.dropna(inplace=True)
    degrons_table['exp_syn'] = degrons_table.apply(lambda v: v['exp_syn'] * v['degron_rate'], axis=1)
    degrons_table['offset'] = degrons_table['exp_syn'].apply(np.log)

    # ...append degrons_table to nbrtable

    nbrtable = pd.concat([nbrtable, degrons_table], sort=False)

    # ...fitting

    formula = 'n_syn ~ exp_syn + ' + ' + '.join([f'PC{i}' for i in range(1, 21)]) + ' + offset - 1'
    resglm, fitting = glm(nbrtable, formula)

    # Likelihood Ratio Test

    analysis = dNdSAnalysisDegron(maf, resglm, degron_site_counts)
    degrons_list = list(analysis.annotmuts['degron'].unique())
    degrons_list.remove('-')
    l_data = list(map(partial(dnds_func, analysis), degrons_list))
    data = pd.concat(l_data, axis=0)
    data.reset_index(inplace=True)
    del data['index']
    data['wmis_degron'] = data.apply(lambda v: infer_dnds(v['wmis_gene'], v['wmis_dc'],
                                                          v['nmis_gene'], v['nmis_dc']), axis=1)
    return data


# Auxiliary methods


def dnds_func(analysis, elem):
    try:
        res = []
        for dc in [False, True]:
            model = analysis.model(elem, degron_complement=dc)
            a = model.positive_selection(free_csqn=['mis', 'other'])
            res.append(a)
        return tuple2dataframe(tuple(res), elem)
    except:
        return np.nan


def tuple2dataframe(item, degron):

    gene = degron.split(':')[-1]
    df_dict = {'degron'   : [degron],
               'gene'     : [gene],
               'nmis_gene': [item[0]['nmis']],
               'wmis_gene': [item[0]['wmis']],
               'llkd_gene': [item[0]['llkd']],
               'p_gene'   : [item[0]['p']],
               'nmis_dc'  : [item[1]['nmis']],
               'wmis_dc'  : [item[1]['wmis']],
               'llkd_dc'  : [item[1]['llkd']],
               'p_dc'     : [item[1]['p']]}
    return pd.DataFrame(df_dict)


def infer_dnds(w_ab, w_a, n_ab, n_a):

    e_ab = ((w_ab - 1) / w_ab) * n_ab
    e_a  = ((w_a - 1) / w_a) * n_a
    e_b = e_ab - e_a
    n_b = n_ab - n_a
    return n_b / (n_b - e_b)


def degron_rate(degron, relfreq, degron2gene):

    degronseq = degroncount[degron]['seq']

    try:
        gene = hugo2ensembl[degron2gene[degron]]
        gene_index = genecount['gene_index'].index(gene)
    except:
        return None

    genecount_matrix = genecount['matrix']

    synindex = genecount['csqn_index'].index('synonymous_variant')
    misindex = genecount['csqn_index'].index('missense_variant')
    nonindex = genecount['csqn_index'].index('stop_gained_variant')

    synsites = genecount_matrix[gene_index, :, synindex]
    missites = genecount_matrix[gene_index, :, misindex]
    nonsites = genecount_matrix[gene_index, :, nonindex]
    mutsites = synsites + missites + nonsites

    gene_intensity = np.dot(relfreq, mutsites)
    degron_intensity = np.dot(relfreq, allcontexts(degronseq))

    return degron_intensity / gene_intensity


def test():
    tumortype = 'BRCA'
    df = run(tumortype)
    df.to_csv(os.path.join(PATH, f'test_degron/results/{tumortype}.results.table.tsv'), sep='\t', index=False)


if __name__ == '__main__':

    for fn in tqdm.tqdm(glob.glob(os.path.join(DEGRONPATH, '*_annotmuts_degron.out.gz'))):
        try:
            tumortype = os.path.basename(fn).split('_')[0]
            df = run(tumortype)
            df.to_csv(os.path.join(PATH, f'test_degron/{tumortype}.results.table.tsv'), sep='\t', index=False)
        except:
            print(tumortype)
